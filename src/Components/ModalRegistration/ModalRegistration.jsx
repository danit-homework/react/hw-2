import { Component } from "react";
import "./ModalRegistration.scss"

class ModalRegistration extends Component {

   render() {
      const { XBtn, closeModal, headerRegModal, textRegModal, actions } = this.props
      return (
         <div className="modal-wrapper" onClick={closeModal}>
            <div className="modal" onClick={e => e.stopPropagation()}>
               <div className="modal-box">
                  {XBtn && <button type="button" className="modal-close" onClick={closeModal}>
                     <svg viewBox="0 0 15 15" width="25px" height="25px">
                        <line x1="3" y1="3" x2="12" y2="12" stroke="white" strokeWidth="1" />
                        <line x1="12" y1="3" x2="3" y2="12" stroke="white" strokeWidth="1" />
                     </svg>
                  </button>}
                  <div className="modal-header">
                     <h4>{headerRegModal}</h4>
                  </div>
                  <div className="modal-content">
                     <p>{textRegModal}</p>
                  </div>
                  <div className="modal-footer">
                     {actions}
                  </div>
               </div>
            </div>
         </div>
      )
   }
}

export default ModalRegistration
