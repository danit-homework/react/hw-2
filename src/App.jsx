
import { Component } from 'react'
import { Button } from "./Components/Button";
import { Modal } from "./Components/Modal";
import Header from './Components/Header/Header';
import Footer from './Components/Footer/Footer';
import { Products } from './Components/Cards';
import { Cards } from './Components/Cards/components/Phones';
// import { sendRequest } from './Helpers/getData';


class App extends Component {
   state = {
      modal: false,
      favorite: JSON.parse(localStorage.getItem('favorites')) || [],
      basket: JSON.parse(localStorage.getItem('basket')) || [],
      currentMachineBasket: {},
      changeColorStarOnclick: false
   }

   openModal = () => {
      console.log('aaa')
      this.setState((prevState) => {
         return {
            ...prevState,
            modal: !prevState.modal
         }
      })
   }

   handlerCurrentMachines = (currentMachineBasket) => {
      this.setState((prevState) => {
         console.log(prevState.currentMachineBasket)
         return {
            ...prevState,
            currentMachineBasket: { ...currentMachineBasket }
         }
      })
   }

   handlerBasket = (currentMachineBasket) => {
      this.setState((prevState) => {
         let aaa = true
         prevState.basket.forEach((el) => {
            if (currentMachineBasket.id === el.id) {
               aaa = false
            }
         })
         if (aaa) {
            localStorage.setItem('basket', JSON.stringify([...prevState.basket, currentMachineBasket]));
            return {
               ...prevState,
               basket: JSON.parse(localStorage.getItem('basket'))
            }
         }
         if (!aaa) {
            return {
               ...prevState,
               basket: JSON.parse(localStorage.getItem('basket'))
            }
         }
      })
   }

   handlerFavorite = (favorite) => {
      this.setState((prevState) => {
         let aaa = true
         prevState.favorite.forEach((el) => {
            if (favorite.id === el.id) {
               aaa = false
            }
         })
         if (aaa) {
            localStorage.setItem('favorites', JSON.stringify([...prevState.favorite, favorite]));
            return {
               ...prevState,
               favorite: JSON.parse(localStorage.getItem('favorites')),
               iconStarColor: true
            }
         }
         if (!aaa) {
            let bbb = JSON.parse(localStorage.getItem('favorites'))
            bbb.forEach((el) => {
               if (el.id === favorite.id) {
                  bbb.splice(bbb.indexOf(el), 1)
               }
            })
            localStorage.setItem('favorites', JSON.stringify(bbb));
            return {
               ...prevState,
               favorite: JSON.parse(localStorage.getItem('favorites')),
               iconStarColor: false
            }
         }
      })
   }

   changeColor = () => {
      console.log('xxxx')
      this.setState((prevState) => {
         return {
            ...prevState,
            changeColorStarOnclick: !prevState.changeColorStarOnclick
         }
      })
   }


   render() {
      const { modal, favorite, currentMachineBasket, basket } = this.state
      return (
         <div>
            <Header countBasket={basket.length} countFavorite={favorite.length} />
            <Products>
               <Cards
                  handlerFavorite={this.handlerFavorite}
                  openModal={this.openModal}
                  handlerBasket={this.handlerCurrentMachines}
                  changeColor={this.changeColor}
                  favorites={favorite} >
               </Cards>
            </Products>
            {modal && <Modal
               XBtn={true}
               closeModal={this.openModal}
               headerRegModal={"Hello"}
               textRegModal={'Do you want to add this item to your basket'}
               actions={<div className="button-wrapper">
                  <Button
                     btnClass={"btn"}
                     text={'ok'}
                     addToBasket={() => this.handlerBasket(currentMachineBasket)} openModalReg={this.openModal}
                  />
                  <Button
                     btnClass={"btn"}
                     text={'cancel'}
                     openModalReg={this.openModal}
                  />
               </div>}
            />}
            <Footer />
         </div>
      )
   }
}

export default App;